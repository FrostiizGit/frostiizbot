module.exports = {
    name: "issue",
    execute(bot, req) {
        let state = req.body.object_attributes.state;
        let project = req.body.project.name;
        let urlProject = req.body.project.web_url;
        let datetime = new Date();
        let title = req.body.object_attributes.title;
        let description = req.body.object_attributes.description;

        bot.createMessage("361468887977426944", {
            embed: {
                color: 4778456,
                timestamp: datetime,
                author: {
                    name: project,
                    url: urlProject,
                    icon_url:
                        "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/1108px-GitLab_Logo.svg.png"
                },
                fields: [
                    {
                        name: `[${state}] ${title}`,
                        value: description
                    }
                ]
            }
        });
    }
};
