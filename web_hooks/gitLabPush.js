module.exports = {
    name: "push",
    execute(bot, req) {
        let author = req.body.user_username;
        let project = req.body.project.name;
        let urlProject = req.body.project.web_url;
        let datetime = new Date();
        let commitMsg = req.body.commits[0].message;

        bot.createMessage("361468887977426944", {
            embed: {
                color: 4778456,
                timestamp: datetime,
                author: {
                    name: project,
                    url: urlProject,
                    icon_url:
                        "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/1108px-GitLab_Logo.svg.png"
                },
                fields: [
                    {
                        name: "New Push by " + author,
                        value: commitMsg
                    }
                ]
            }
        });
    }
};
