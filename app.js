// Discord Bot dependencies
const fs = require("fs");
const _config = require("./config.json");
const prefix = _config.prefix;
const Eris = require("eris");
// WebHookServer dependencies
const express = require("express");
const bodyParser = require("body-parser");

let bot = new Eris(_config.bot_token);
const app = express();
bot.commands = new Eris.Collection();
const commandFiles = fs
    .readdirSync("./commands")
    .filter(file => file.endsWith(".js"));

for (const file of commandFiles) {
    const command = require(`./commands/${file}`);
    console.log(`Added command: ${file}`);
    // Set a new item in the Collection
    // with the key as the command name and the value as the exported module
    bot.commands.set(command.name, command);
}

bot.queue = new Array();
bot.dbmPull = 0;

// WebHooks
let webhooksCollection = new Eris.Collection();
const webhookFiles = fs
    .readdirSync("./web_hooks")
    .filter(file => file.endsWith(".js"));
for (const file of webhookFiles) {
    const command = require(`./web_hooks/${file}`);
    console.log(`Added webhook: ${file}`);
    // Set a new item in the Collection
    // with the key as the command name and the value as the exported module
    webhooksCollection.set(command.name, command);
}
var jsonParser = bodyParser.json();

bot.on("ready", () => {
    console.log("Ready!");
});
bot.on("messageCreate", msg => {
    // If msg doesn't start with prefix or sent by bot, exit early.
    if (!msg.content.startsWith(prefix) || msg.member.bot) return;

    // Slice the prefix & then splits the msg into an array by spaces
    const args = msg.content.slice(1).split(/ +/);
    // Shift takes the first element in the array & also removes it
    const commandName = args.shift().toLowerCase();

    if (!bot.commands.has(commandName)) return;

    const command = bot.commands.get(commandName);

    if (command.args && !args.length) {
        return bot.createMessage(
            msg.channel.id,
            `You didn't provide any arguments, ${msg.member.user.mention}!`
        );
    }

    try {
        command.execute(bot, msg, args);
    } catch (error) {
        console.error(error);
        bot.createMessage(
            msg.channel.id,
            "There was an error trying to execute that command!"
        );
    }
});

bot.on("error", err => {
    console.log("Client error: ", err);
});

bot.connect();

// WebHookServer
app.post("/webhook/gitlab", jsonParser, (req, res) => {
    res.status(200).send("OK");
    const event = req.body.object_kind;
    const hook = webhooksCollection.get(event);
    hook.execute(bot, req);
});

app.listen(_config.port, _config.hostname, () => {
    console.log(
        `Server running at http://${_config.hostname}:${_config.port}/`
    );
});
