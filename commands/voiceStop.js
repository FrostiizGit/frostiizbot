module.exports = {
    name: "stop",
    description: "Skip/stop the current song or stream!",
    cooldown: 5,
    execute(bot, msg, args) {
        // Get voiceConnection from the collection
        bot.voiceConnections.find(voiceConnection => {
            voiceConnection.stopPlaying();
        });
    }
};
