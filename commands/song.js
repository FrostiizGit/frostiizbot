module.exports = {
    name: "song",
    description: "Tell a user the song currently playing by the bot",
    cooldown: 5,
    execute(bot, msg, args) {
        msg.delete();
        bot.createMessage(
            msg.channel.id,
            `${msg.member.user.mention} the current song: ${bot.currentSong}`
        );
    }
};
