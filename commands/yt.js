const yt = require("ytdl-core");
module.exports = {
    name: "yt",
    description: "Ping!",
    args: true,
    cooldown: 5,
    execute(bot, msg, args) {
        msg.delete();
        // Check if user is in voice channel
        if (msg.member.voiceState.channelID === null) {
            bot.createMessage(
                msg.channel.id,
                "Baka " +
                    msg.member.user.mention +
                    " ! You must be in a voice channel 🤬"
            );
            return;
        }

        // Get voiceConnection from the collection
        let voiceCon = bot.voiceConnections.find(voiceConnection => {
            return voiceConnection;
        });

        // If none then join the user
        if (!voiceCon) {
            bot.joinVoiceChannel(msg.member.voiceState.channelID)
                .then(connection => {
                    // Save the newly create voice connection
                    voiceCon = connection;
                    songManager(bot, msg, args[0], voiceCon);
                })
                .catch(err => {
                    bot.createMessage(
                        msg.channel.id,
                        "Error joining voice channel: " + err.message
                    );
                    console.log(err);
                });
        } else {
            if (!voiceCon.playing) {
                // Join user
                bot.joinVoiceChannel(msg.member.voiceState.channelID)
                    .then(connection => {
                        // Save the newly create voice connection
                        voiceCon = connection;
                        songManager(bot, msg, args[0], voiceCon);
                    })
                    .catch(err => {
                        bot.createMessage(
                            msg.channel.id,
                            "Error joining voice channel: " + err.message
                        );
                        console.log(err);
                    });
            } else {
                songManager(bot, msg, args[0], voiceCon);
            }
        }
    }
};

function songManager(bot, msg, url, voiceCon) {
    // If playing add to queue if possible else play directly
    if (voiceCon.playing) {
        // Checking if queue is full
        if (!isQueueFull(bot, msg)) {
            //Validate url before adding
            if (yt.validateURL(url)) {
                // Queue is not full then add url to the queue
                bot.queue.push(url);
                createEmbed(bot, msg, url);
            } else {
                bot.createMessage(
                    msg.channel.id,
                    `You must enter a valid youtube link ${
                        msg.member.user.mention
                    }`
                );
            }
        }
    } else {
        if (yt.validateURL(url)) {
            // Bot is not playing any voice then play audio
            playYt(voiceCon, url, bot, msg);
            createEmbed(bot, msg, url);
        } else {
            bot.createMessage(
                msg.channel.id,
                `You must enter a valid youtube link ${msg.member.user.mention}`
            );
        }
    }
}

function playYt(connection, urlToVid, bot, msg) {
    yt.getInfo(urlToVid, (err, info) => {
        if (err) throw err;
        let { title } = info;
        connection.play(
            yt(urlToVid, {
                requestOptions: { maxRetries: 8, maxReconnects: 8 }
            })
        );
        bot.currentSong = title;
        onEnd(bot, msg, connection, title);
    });
}

function isQueueFull(bot, msg) {
    if (bot.queue.length > 9) {
        bot.createMessage(
            msg.channel.id,
            "Queue is already full, please try again later !"
        );
        return true;
    }
    return false;
}

function checkQueue(bot) {
    if (bot.queue.length > 0) {
        return true;
    }
    return false;
}

function onEnd(bot, msg, connection, title) {
    connection.once("end", () => {
        bot.createMessage(msg.channel.id, `Finished **${title}**`);
        if (checkQueue(bot)) {
            playYt(connection, bot.queue.shift(), bot, msg);
        } else {
            bot.currentSong = "Not playing any song.";
        }
    });
}

function createEmbed(bot, msg, url) {
    yt.getInfo(url, (err, info) => {
        if (err) throw err;
        let { title } = info;
        bot.createMessage(msg.channel.id, {
            embed: {
                title: "<a:rainbowPls:543741686598205442> " + title,
                description: "Link: " + url,
                color: 0xff0000,
                footer: {
                    text:
                        "Added by " +
                        msg.author.username +
                        " (Queue " +
                        bot.queue.length +
                        ")"
                }
            }
        });
    });
}
