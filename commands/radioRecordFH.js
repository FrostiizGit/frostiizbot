module.exports = {
    name: "fh",
    description: "Radio Record - Future house!",
    cooldown: 5,
    execute(bot, msg, args) {
        msg.delete();
        const textChannelForUpdate = msg.channel.id;
        bot.joinVoiceChannel(msg.member.voiceState.channelID)
            .catch(err => {
                // Join the user's voice channel
                bot.createMessage(
                    msg.channel.id,
                    "Error joining voice channel: " + err.message
                ); // Notify the user if there is an error
                console.log(err); // Log the error
            })
            .then(connection => {
                if (connection.playing) {
                    // Stop playing if the connection is playing something
                    connection.stopPlaying();
                }
                connection.play("http://air.radiorecord.ru:805/fut_320"); // Play the file and notify the user
                bot.createMessage(
                    msg.channel.id,
                    `Now playing **Radio Record Future House**`
                );
                connection.once("end", () => {
                    bot.createMessage(
                        msg.channel.id,
                        `Finished playing **Radio Record Future House**`
                    );
                });
            });
    }
};
