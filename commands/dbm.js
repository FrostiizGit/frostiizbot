module.exports = {
    name: "dbm",
    description: "Tell a user the song currently playing by the bot",
    cooldown: 5,
    execute(bot, msg, args) {
        msg.delete();
        let fileToPlay = "";
        if (args[0] == "10") {
            fileToPlay = "./media/dbm_sound/dbm10.mp3";
            bot.dbmPull += 1;
        } else if (args[0] == "gg") {
            fileToPlay = "./media/dbm_sound/VictoryLong.mp3";
        } else if (args[0] == "count") {
            bot.createMessage(msg.channel.id, "Pull count: " + bot.dbmPull);
        } else if (args[0] == "reset") {
            bot.dbmPull = 0;
        } else {
            return;
        }

        // Check if user is in voice channel
        if (msg.member.voiceState.channelID === null) {
            bot.createMessage(
                msg.channel.id,
                "Baka " +
                    msg.member.user.mention +
                    " ! You must be in a voice channel 🤬"
            );
            return;
        }

        // Get voiceConnection from the collection
        let voiceCon = bot.voiceConnections.find(voiceConnection => {
            return voiceConnection;
        });

        // If none then join the user
        if (!voiceCon) {
            bot.joinVoiceChannel(msg.member.voiceState.channelID)
                .then(connection => {
                    // Save the newly create voice connection
                    voiceCon = connection;
                    voiceCon.play(fileToPlay);
                })
                .catch(err => {
                    bot.createMessage(
                        msg.channel.id,
                        "Error joining voice channel: " + err.message
                    );
                    console.log(err);
                });
        } else {
            voiceCon.stopPlaying();
            voiceCon.play(fileToPlay);
        }
    }
};
