module.exports = {
    name: "leave",
    description: "Leaving voice channel !",
    cooldown: 5,
    execute(bot, msg, args) {
        msg.delete();
        // Collection magic :pepehands:
        console.log(
            bot.voiceConnections.find(voiceConnection => {
                bot.getChannel(voiceConnection.channelID).leave();
            })
        );
    }
};
