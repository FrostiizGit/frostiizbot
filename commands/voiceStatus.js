module.exports = {
    name: "status",
    description: "Skip/stop the current song or stream!",
    cooldown: 5,
    execute(bot, msg, args) {
        // Get voiceConnection from the collection
        let voiceCon = bot.voiceConnections.find(voiceConnection => {
            return voiceConnection;
        });

        if (voiceCon) {
            console.log("There is a voice conneciton");
        } else {
            console.log("No connection found");
        }
    }
};
