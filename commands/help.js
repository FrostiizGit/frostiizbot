module.exports = {
    name: "help",
    description: "List all the available function by the bots",
    cooldown: 5,
    execute(bot, msg, args) {
        msg.delete();
        bot.createMessage(msg.channel.id, {
            content: frostiizDetection(msg),
            embed: {
                title: "COMMAND LIST",
                description: "A short list of the commands available",
                color: 4778456,
                fields: [
                    {
                        name: ".help",
                        value: "Shows this message"
                    },
                    {
                        name: ".fh",
                        value: "Plays Radio Record - Future House radio"
                    },
                    {
                        name: ".yt youtubeLink",
                        value:
                            "Plays a youtube video, queue max size is 10, doesn't support playlists link"
                    },
                    {
                        name: ".stop",
                        value:
                            "Allows to skip to the next song or end the stream if there are no queue."
                    },
                    {
                        name: ".leave",
                        value: "Disconnect the bot from a voice channel"
                    },
                    {
                        name: ".song",
                        value: "Tells you the song currently playing"
                    }
                ]
            }
        });
    }
};

function frostiizDetection(msg) {
    if (msg.member.id == "76471963953926144") {
        return msg.member.user.mention + " senpai <a:lickr:552859710358028299>";
    } else {
        return (
            "Always here to help you " +
            msg.member.user.mention +
            "<:kappapleb:450734772805828638> Fucking pleb <:lul:381186853279236099>"
        );
    }
}
