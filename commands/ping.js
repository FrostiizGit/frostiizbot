module.exports = {
    name: "ping",
    description: "Ping!",
    cooldown: 5,
    execute(bot, msg, args) {
        bot.createMessage(msg.channel.id, "Pong.");
    }
};
